---
title: 'Idealista Developer Week, Cultura de equipo'
cover: /images/2016/09/idealista-developer-week-cover.jpg
cover_caption: Ofinas de Idealista, Madrid
id: 201
categories:
  - Empresa
  - Team
  - Events
  - idealista
date: 2016-09-17 08:00:00
tags:
---

El 12 de septiembre, este lunes, fue el día 256 del año o lo que es lo mismo el **Día del Programador**. Con motivo de esto, [idealista](https://twitter.com/idealista/status/775234977294344192) nos ha preparado una semana muy entretenida, de esas que demuestran una buena cultura de equipo.

El lunes, al llegar a la oficina teníamos golosinas esperándonos, además de varios carteles y pósters de varios tipos: chistes, atajos de teclado y alguno que vaticinaba lo que se nos venía encima, ese día se nos dijo que creáramos equipos de dos o tres personas y que al final del día recibiríamos instrucciones para la competición del resto de la semana.


A la mañana siguiente ya empezamos toda una serie de pruebas que nos iban hacer conseguir puntos. Las pruebas iban desde intentar comerse 4 galletas en un minuto, hasta averiguar varios huevos de pascua de diferentes webs. Además de las pequeñas pruebas tuvimos algunas competiciones, el martes hicimos una competición de [FrikiTrivial](http://www.frikitrivial.com/), nuestra puntuación fue nefasta, pero pasamos un buen rato.

Otro día significativo fue el miércoles, **hicimos cambios de equipos**, parejas de diferentes equipos dentro del departamento de tecnología, en mi caso, que estoy dentro del equipo de profesionales, me toco con una persona del equipo de Soporte y pasamos la mañana trabajando juntos y viendo más de cerca su trabajo y luego él paso la tarde en mi equipo.

El resto de días de la semana lo pasamos entre competiciones, tuvimos una competición de averiguar ciertos datos sobre varios lenguajes, la tarde del jueves la dedicamos a hacer un [RoboCode](http://robocode.sourceforge.net/) para competir el viernes en una batalla memorable y por último, después de la batalla, nos pasamos el resto del día divididos por equipos haciendo una implementación para un cliente de la [API pública de idealista](http://developers.idealista.com/access-request). Cada equipo tenía un lenguaje y el lunes tendremos una _"language war"_, para ver que cliente tarda menos en hacer mil peticiones iguales.


En general ha sido una semana diferente, creo que ayuda muchísimo a integrar un equipo de muchas personas (no se cuantas, supongo que entre 60 y 80) que trabaja en un ecosistema que aunque mucha gente no lo sabe, va más allá de la web.

_"El equipo humano de una empresa es su mayor valor"_