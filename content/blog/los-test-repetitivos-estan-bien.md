---
title: "Los test repetitivos estan bien."
date: 2022-04-06 15:59:21
id: 211
categories:
  - Testing
  - Opinion
  - Golang
  - Programming
tags:
---
This article is my opinion and it follows the _[just sharing](https://justsharing.dev/) principle_.

Contenido originalmente publicado como [hilo de twitter](https://twitter.com/juanvegadev/status/1511614506438316037)

Table testing es una de las "buenas" prácticas establecidas en cuanto a testing en la comunidad #golang. Incluso encontramos referencias en la documentación oficial. Y por eso he empezado a odiar table testing desde que programo en Go 🧵 

https://go.dev/blog/subtests

Data-driven testing, table testing, parametrized test e incluso Example based testing... Todo son formas de llamar a la misma idea de reusar un "spec" cambiando el input para poder verificar diferentes escenarios, y es una herramienta genial hasta que pierde el foco.

El afán de reusar hace que muchas personas lleven el table testing al extremo y algunos "spec" tengan tantos parámetros que básicamente podría estar probando cualquier función. Puedes cambiar el SUT sin cambiar el test. Un éxito hasta que necesitas entender porque el test se ha roto, y te das cuenta que ese test no está aislado, y contiene casi tanta lógica como el código de producción. ¿Quién vigila al vigilante?

Tenemos que entender que está bien aplicar todo los principios de calidad al código de test, incluido DRY, pero DRY habla principalmente de conceptos y reglas de negocio no de "asserts" repetidos. Un test (unitario) tiene que ser:

- Rápido
- Independiente - Aislado de su entorno, e.g. orden de ejecución
- Reproducible
y es casi perfecto si es más fácil de mantener y leer que el SUT.
Además, personalmente considero que cuanto más Social Testing mejor. 😄

*FIRST principle

Fijense en este ejemplo, lo que prueba el test está claro. La interfaz del SUT es evidente y nos ahorra muchas líneas, aquí data-driven testing nos ayuda a cumplir lo anterior y potencia nuestra suite.

![](/img/table-testing-1.png)

Sin embargo, aquí es donde falla el table testing como herramienta para aplicar DRY. Cuesta entender muchas cosas, existe branching dentro del test y requiere una carga congnitica mucho mayor.

![](/img/table-testing-2.png)

Los test parametrizados son una de mis herramientas favoritas, pero como todas, hay que saber donde aplicarlas. Además, casi nunca hago TDD más parametrized test. Normalmente, en una segunda vuelta con todo mi código terminado tiendo a intentar generalizar algunos casos.