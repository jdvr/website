---
title: "Start doing better code reviews tomorrow: The emoji code "
date: 2022-03-30 10:00:21
id: 210
categories:
  - career
  - good practices
  - code reivew
  - Programming
tags:
---

This article is my opinion and it follows the _[just sharing](https://justsharing.dev/) principle_.

One of the software engineer’s main activity nowadays is providing peer feedback about code, but giving written feedback is hard. You mainly face two issues:

- Keep it short and straightforward.
- Keep your intention clear.

## It's all about your intention

We work with other people, and we usually don't want to hurt them or make them feel overwhelmed. To help here, and inspired by [a Microsoft post](https://devblogs.microsoft.com/appcenter/how-the-visual-studio-mobile-center-team-does-code-review/), we introduced the **emoji code for reviews** within our team.


> Make your comment intention crystal clear with an emoji.


At the beginning of each comment, you include an emoji that express your intention with that comment.

## Our code

There are several public examples: [Code Review Emoji Guide](https://github.com/erikthedeveloper/code-review-emoji-guide), [My Code Review Emoji Guide](https://brendanforster.com/notes/my-code-review-emoji-guide/) and [Emoji Code Review Comments](https://gist.github.com/chrisriesgo/818fe94b4f4720eaf7898ccaa48f94c5). In my team we use the following one:

- 👍: I love this part and want to praise it!
- ❓: I have a question or need some insights to understand this part.
- 🐛: This is a potential bug. You must provide a detailed explanation and, if possible, a suggested change.
- 🤡: A matter of taste, it doesn't affect quality, but from the reviewer's point of view, it will improve code
- 💭: Not action required. I want to share a thought, to seed an idea for a future debate. Even not an answer is required.
- ♻️: Propose a refactor to simplify or improve a code block. The suggested change is required at least as pseudo-code.

It is short, and you can memorize it at first sight. Anyone can understand what it means, even without checking the legend.

## Next step

You can start using this independently or start by proposing any of the existing examples to your team. Don't add too many emojis to the code. Make it easier to remember and be pragmatic.
