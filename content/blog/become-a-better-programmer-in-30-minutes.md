---
title: Become a better programmer in 30 minutes
date: 2022-02-16 15:59:21
id: 209
categories:
  - career
  - good practices
  - code reivew
  - Programming
tags:
---
This article is my opinion and it follows the _[just sharing](https://justsharing.dev/) principle_.

Well, the title is a little tricky. It will take you 30 minutes every day, but I promise, it is effortless.

> "I'm not a great programmer; I'm just a good programmer with great habits."
― Kent Beck 

## How do the first 30 minutes of your day usually look?

Like most software engineers, I usually spend over 60% of my workday (about 5 hours) coding. So **taking away 30 minutes means nothing** on the total time of the day and the project's whole time.

You might wonder _"why the first 30 minutes and not the last, or somewhere in the middle?"_. Easy, **the actions that I propose take much energy and focus when done well, so it's better to do them when you are still fresh!**

## My 30 minutes

All this practice works well for my current context and environment. You can **start copying me and looking for new ones or adapted versions that fit you.**

### Self code reviews

I always start the day by doing a code review of the last code that I  wrote. I asked myself what I was trying to achieve, how well the code expressed it, and what comments or clarification an external reviewer might need to understand my decisions. So, I have three steps:

1. What was the intention here? **(Intention)**
2. How well is the code expressing it? **(Naming)**
3. Which comments will help an external reviewer to understand it? **(Comment)**

Once I have answered the questions, I start again, using the answers to rethink intention, naming, and comments. For instance, I ask myself: **How can I change code naming to include the value of the additional comment for an external reviewer?**


![small kotlin snippet with arrows pointed to some code where I could apply the previous questions](/img/208-review-sample.png)

### Starting from scratch

There are always at least two ways of coding the same thing. A good exercise is to ask me every day: What if I use the other approach I discarded?

**I don't implement it**. I usually spend some minutes checking which file would change. The goal is to figure out if I could improve my current solution by evaluating it from another point of view. **I might don't need to change a single line of code.**

Among the previous exercise, I also wonder:
1. How much code do I write to solve the actual problem? Usually, the simpler the code is, the easier it will be to change if required. **Sometimes we overcomplicate the solutions to cover future use cases that might never arrive.**
2. Change some tests data and code pseudorandomly. I don't do [mutation testing](https://en.wikipedia.org/wiki/Mutation_testing) with an automated tool, but I manually **delete some lines and change some inputs and expected values to verify how the tests break**.

**Do you dare to test this tomorrow and tell me how it well?**

>"No matter the circumstance you can always improve. You can always start improving with yourself. You can always start improving today."
― Kent Beck, Extreme Programming Explained: Embrace Change 

## References
Firstly, I hear from this practice, with a different approach, of reviewing previous day code from [Carlos Blé](carlosble.com)

[Photo by Ivan Samkov from Pexels](https://www.pexels.com/photo/wood-art-creative-wall-5799100/)