---
title: 'Aprendiendo a aprender: Apprenticeship Patterns'
id: 168
categories:
  - Books
  - Programming
date: 2015-09-27 12:52:51
tags:
---

En el camino hacia la maestría el primer paso es ser un aprendiz, y en esa etapa es importante tener buenas prácticas para que nuestro camino como aprendices sea cómodo y fructífero, mis compañeros y yo hemos comenzado el club de lectura leyendo [Apprenticeship Patterns](http://shop.oreilly.com/product/9780596518387.do), dado que nuestro objetivo es aprender, era importante comenzar con un libro que nos ayudará a identificar como podíamos sacar mejor provecho de nuestras lecturas futuras y de nuestro trabajo diario en general.
<!-- more -->
&nbsp;

## Resumen general del libro

El libro esta dividido en capítulos como cualquier otro, pero sin embargo no esta pensando para ser leído desde inicio a fin como un cuento o una historia lineal, se pueden leer los patrones de manera independiente y aún así se aprovechan, sin embargo, nosotros hemos leído el libro en un orden tradicional.

En cada capítulo encontramos una serie de patrones que están relacionados entre sí, además de esta relacionados con el resto de patrones del libro. Los autores han identificado, ordenado y luego escrito de forma clara las situaciones que se le presentan a un aprendiz en el inicio de su carrera, a cada situación, con un contexto y un problema le acompaña una solución y unas acciones.

## 

## Opinión personal

Desde mi punto de vista los autores han tenido mucho acierto identificando los diferentes problemas y creando patrones que aportan una solución y acciones que cualquiera puede aplicar de forma casi inmediata en su vida diaria (depende del patrón), yo destacaría el gran número de citas y frases celebres que menciona el libro, algunas de autores del mundo de la ingeniería del software y otras de libros de ámbitos completamente ajenos a nuestra profesión. Pero siempre frases que son capaces de trasmitir el mensaje de ese patrón o capítulo.

Si bien es cierto que para mi los patrones identifican problemas reales, en algunos momentos da la sensación que las soluciones aportadas nos son concisas, es decir, los autores se lían un poco intentando detallar demasiado la solución y dando ejemplos, aún así creo que los patrones tiene mucho fundamento y las soluciones y acciones en muchos casos ya las he llevado a mi vida real.

En general, todos mis compañeros y yo hemos sentido que todos los patrones llevaban al buen camino, y también nos hemos alegrado de notar que muchas de las soluciones y acciones que se proponen ya forman parte de nuestra rutina diaria.

&nbsp;

## A cada problema, le sigue una solución

Como he mencionado los patrones contiene un contexto y un problema, seguido de una solución y una acción, quería destacar las partes en las que el libro ha afectado de manera inmediata en mi vida diaria, casualmente leí el patrón _**Empty Cup**_ mientras esperaba para comenzar mi primer día de trabajo en nueva empresa, el libro me aportó la chispa que me faltaba para darme cuenta de lo importante que es escuchar activamente a alguien, escuchar como parte de la conversación, me di cuenta que en muchas ocasiones soy yo mismo el que pone el freno para que la otra parte pueda expresar todas sus ideas sin verse afectadas por mis comentarios, desde ese día voy mejorando y soy mas cociente de la importancia del silencio en las conversaciones y de la verdadera escucha activa, he aprendido que en silencio también aporto muchísimo al curso de la conversación.

Otro elemento que no pertenece a un capítulo ni  a un patrón concreto es la referencia que hace el libro a la importancia de escribir, guardar, "mapear" y compartir tu conocimiento, desde hace casi 4 meses intento escribir semanalmente en el blog, este proceso comenzó como ejercicio para mejorar mi capacidad para expresar mis ideas de forma escrita, durante la lectura del libro un día me paré a leer todos los post que había escrito y me di cuenta de como en unos pocos meses he ido mejorando mi forma de expresarme y como el primer post me costo casi una semana escribirlo y ahora soy capaz de escribir post más largos, con mejor estructura y  sintiéndome más cómodo.

Otro patrón que tuvo impacto directo en mi vida como desarrollador fue _**Craft Over Art**_, me recordó que mi trabajo y mi profesión se trata de entregar un software funcional que satisfaga las necesidades de un cliente, hacer buen código para mi siempre es importante pero también es verdad que no se debe perder nunca la entrega continua de valor, porque sino, el trabajo habrá dejado de tener su sentido inicial.

#### 

## Conclusiones

Me he alegrado de ser capaz de encontrar un lugar y un momento en mi rutina diaria donde aplicar los conceptos del libro, hace poco un amigo mencionaba la  importancia de leer un libro en el momento adecuado puede marcar la diferencia entre que te guste o no. Recomiendo leer este libro a aquellos que están decididos a comenzar su camino como aprendices del Software Craftsmanship, o como nosotros se encuentre en el camino del aprendizaje.

Para un resumen de cada uno de los patrones recomiendo leer a mis colegas:

Ronny: [Apprentices Patterns](http://ronnyancorini.es/blog/index.php/2015/09/07/apprentices-patterns/)

Miguel: [Apprentices Patterns](http://miguelviera.com/blog/apprenticeship-patterns-a-guidance-for-aspiring-software-craftman/)

&nbsp;