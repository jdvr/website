---
title: "Typeform Navigator: My Raycast Extension"
date: 2022-09-05 15:59:21
id: 210
categories:
  - Pet
  - Typeform
  - React
  - Programming
tags:
---

I discovered [Raycast](https://raycast.com) a few months ago, at first sight I notice the power of extensions. You can create automation and handy commands for everything.

I am a Software Engineer at Typeform, so my day to day use of the platform is sightly different to a standard user or customer. I have a lot of workspaces, some shared, some private and within them I have a lot of testing forms, each for every use case I usually work with. So, I have created [Typeform Navigator](https://www.raycast.com/jdvr/typeform).

## Typeform Navigator

Typeform Navigator allows you to easily check form main stats, navigate to workspace page or to a form results page. There are other handy commands like copy form filling url or form ID.

![Screenshot of form overview on Typeform Navigator](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/xnq7y7hr91ti6hwpzrmc.png)


![Screenshot of workspace forms listing on Typeform Navigator](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/8l846zx1xcb7dsed7x8m.png)

## Create a Raycast extension

Typeform Navigator is a nice example of raycast extension because it includes, listing, detail and it requires configuration (a personal token). You can found the source code on my github: [jdvr/typeform-navigator](https://github.com/jdvr/typeform-navigator).

Developer experience is a key advantage of Raycast you can check their [Developer Documentation](https://developers.raycast.com/), starting a new extension is really easy.

The only pain point is publishing in on their storage because you need to create a pull request to their [extensions repository](https://github.com/raycast/extensions/pull/1261).

If you are a Typeform user, test the extension and let me know what do you think.


The project was part of Hive Sprint, a two weeks hackathon where Typeform employees have freedom to spend time on any initiative that might improve the platform or the product.