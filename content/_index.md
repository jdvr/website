+++
title = "Juan D. Vega | Software Engineer"
date = "2014-04-09"
menu = "main"
+++


I am a product-focused Software Engineer that believes that a key element for a software product to success is technical excellence that among other things, it includes: pragmatism, good communication skills, deep understanding of business problems, and well-sized solution.

To give well-known names, I love XP and Lean and I think they fit well on DevOps culture to create the best value stream for successful iterative product development. TDD, Trunk-based development, pair programming, iterative and incremental development. I a few words, do the right this, and do it in the right way.

I graduated as Software Engineer by the University Of Las Palmas de Gran Canaria, but this is just the base to build a wide variety of knowledge that I have learned on m own. Languages, frameworks, tools, and practices that I learned through my career thanks to my restless spirit.

:warning: **Disclaimer:** Most blog posts are old and out of date, please, put perspective in what you are reading and on which date it was written. I maybe think differently now. This blog was a tool to improve my writing skill.
